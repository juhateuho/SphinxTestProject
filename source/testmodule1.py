"""This is test module #1.

This module does nothing really since it is for documentation test purposes.

.. moduleauthor:: Juha Teuho <juha.teuho@gmail.com>
"""

class Animal:
	"""Base-class for all animals.

	"""

	def __init__(self, name):
		"""Initialize animal
		
		:param name: Name of the animal.
		:type name: str.

		"""
		self._name = name

	@property
	def name(self):
		"""Get name of the animal

		:returns: str -- name of the animal.

		"""
		return self._name

	@name.setter
	def name(self, name):
		"""Set name for animal

		:param name: Animal's name.
		:type name: str.

		"""
		self.name = name

class Cat(Animal):
	"""Class for cat animal type.

	"""

	def __init__(self,name):
		"""Initialize cat

		:param name: Name of the animal.
		:type name: str.
		"""

		super().__init__(name)

	def eat(self):
		"""Cat-spesific eat-method.

		Prints the way how cat eats.

		"""

		print("This is how cat eats.")

class Dog(Animal):
	"""Class for dog animal type.

	"""

	def __init__(self,name):
		"""Initialize dog

		:param name: Name of the animal.
		:type name: str.
		"""

		super().__init__(name)

	def eat(self):
		"""Dog-spesific eat-method.

		Prints the way how cat eats.

		"""

		print("This is how dog eats.")


