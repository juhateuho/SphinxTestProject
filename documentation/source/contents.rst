.. Test project documentation master file, created by
   sphinx-quickstart on Mon Apr 13 18:08:36 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Test project's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rst/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
